package com.ciceksepeti.demo.service;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.springframework.stereotype.Component;

import static io.restassured.RestAssured.baseURI;

@Component
public class RequestBaseService {

    public io.restassured.specification.RequestSpecification requestSpecification() {
        baseURI = "https://b95a934d-b7fe-49b0-96c9-d70e636880bc.mock.pstmn.io";
        RequestSpecification requestSpecification = RestAssured.given();
        requestSpecification.header("Accept", "application/json");
        requestSpecification.header("Content-Type", "application/json");
        return requestSpecification;
    }
}

