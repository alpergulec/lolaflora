package com.ciceksepeti.demo.base;

import org.junit.AfterClass;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;

import java.io.FileInputStream;
import java.util.Properties;

import static java.lang.ThreadLocal.withInitial;

@Component
public class DriverFactory {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    public static Properties prop;
    DriverType selectedDriver = DriverType.CHROME;
    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    public DriverFactory(){}

    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance(){
        return instance;
    }

    ThreadLocal<WebDriver> driver = withInitial(() -> selectedDriver.getWebDriverObject(desiredCapabilities));

    public WebDriver getDriver(){
        return driver.get();
    }
    public Properties properties(){
        try {
            prop = new Properties();
            FileInputStream fileInputStream= new FileInputStream(System.getProperty("user.dir")+"/src/main/java/config/config.properties");
            prop.load(fileInputStream);
            return prop;

        }catch (Exception e){
        e.printStackTrace();
        }
        return null;
    }

    @AfterClass
    public void removeDriver(){
        driver.get().close();
        driver.get().quit();
    }
}
