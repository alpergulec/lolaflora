package com.ciceksepeti.demo.base;

public abstract class AbstractPage {
    public void navigate (final String url){
        DriverFactory.getInstance().getDriver().navigate().to(url);
    }

}
