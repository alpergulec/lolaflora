package com.ciceksepeti.demo.test.api;

import com.ciceksepeti.demo.service.RequestBaseService;
import groovy.util.logging.Slf4j;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Description;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApiTest {

    @Autowired
    private RequestBaseService requestBaseService;

    private static final Logger log = LoggerFactory.getLogger(ApiTest.class);
    private List<String> installment;

    public void lolaFloara(String installment) throws JSONException {

        RequestSpecification requestSpecification = requestBaseService.requestSpecification();
        requestSpecification.queryParam("installment", installment);
        Response response = requestSpecification.get("/test");
        int statusCode = response.getStatusCode();

        if (installment.equals("0") ) { // installment 0 queryParams'da taksit seçeneği olmamalı olarak belirtilmişti. Ancak taksit seçeneği olan ürünler olduğu için hata alınmaktadır.

            JSONArray jsonArray = getJsonArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject finalObject = jsonArray.getJSONObject(i);
                boolean value = finalObject.getBoolean("installment");
                String code = finalObject.getString("code");
                if (value != false) {

                    log.error(" [ ERROR ] [ WHEN INSTALLMENT PARAMS 0 IS SENT, THERE SHOULD BE NO INSTALLMENT OPTION IN THE RESPONSE ] [ ERROR PRODUCT INDEX ] : {} [ CODE OF ERROR PRODUCT ]", i, code);
                }
                Assert.assertFalse(value);
            }
        } else if (installment.equals("1")) {
            JSONArray jsonArray = getJsonArray(response);
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject finalObject = jsonArray.getJSONObject(i);
                boolean value = finalObject.getBoolean("installment");
                Assert.assertTrue(value);
            }
        } else if (installment.equals("")) {
            Assert.assertEquals(500, statusCode);
        }
    }

    private JSONArray getJsonArray(Response response) throws JSONException {

        JSONObject parentObject = new JSONObject(response.asString());
        JSONObject pageName1 = parentObject.getJSONObject("result");
        JSONObject pageN = pageName1.getJSONObject("data");
        return pageN.getJSONArray("products");
    }

    @Test
    @Description("Get api with 0 installment params")
    public void installment0() throws JSONException {
        lolaFloara("0");
    }

    @Test
    @Description("Get api with 1 installment params")
    public void installment1() throws JSONException {
        lolaFloara("1");
    }

    @Test
    @Description("Get api with null installment params")
    public void installmentNull() throws JSONException {
        lolaFloara("");
    }

}
