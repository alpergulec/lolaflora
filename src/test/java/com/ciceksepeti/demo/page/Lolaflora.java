package com.ciceksepeti.demo.page;

import com.ciceksepeti.demo.base.AbstractPage;
import com.ciceksepeti.demo.base.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Lolaflora extends AbstractPage {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    String lolafloraURL = "https://www.lolaflora.com/login";
    DriverFactory driverFactory = new DriverFactory();
    private WebDriver driver = DriverFactory.getInstance().getDriver();

    public void launchLolaflora() {
        super.navigate(lolafloraURL);
        logger.info("Launch lolaflora login page " + lolafloraURL);
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void isDisplayMessage(By errorMessage) {
        WebDriverWait wait = new WebDriverWait(driver, 2);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(errorMessage));
        driver.findElement(errorMessage).isDisplayed();
    }

    public void sendEmail(By email) {
        driver.findElement(email).sendKeys(driverFactory.properties().getProperty("eMail"));
    }

    public void sendPassword(By email) {
        driver.findElement(email).sendKeys(driverFactory.properties().getProperty("password"));
    }

    public void sendIncorrectPassword(By incorrectPassword) {
        driver.findElement(incorrectPassword).sendKeys(driverFactory.properties().getProperty("inCorrectPassword"));
    }

    public void showPopUp(By element) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 5);
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(element));
        String messageElement = driver.findElement(element).getText();
        System.out.println(messageElement);
        Assert.assertEquals("E-mail address or password is incorrect. Please check your information and try again.", messageElement);
    }

    public void closePopUp(By element) {

        driver.findElement(element).click();
    }

}
